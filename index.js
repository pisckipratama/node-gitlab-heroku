const express = require('express');
const fs = require('fs');
const bodyParser = require('body-parser');
const dotenv = require('dotenv');

dotenv.config();

// parse data
let parse = fs.readFileSync('data.json', 'utf-8');
let data = JSON.parse(parse);

// Our function which adds two numbers and returns the result
const addNumbers = (firstNumber, secondNumber) => {
  //   check that input is a number
  if (typeof (Number(firstNumber)) !== 'number' || typeof (Number(secondNumber)) !== 'number') {
    return 'Values should be integer or numbers'
  }
  return Number(firstNumber) + Number(secondNumber);
}

// Destructure our bodyParser methods
const { urlencoded, json } = bodyParser;
const PORT = process.env.PORT || 9000;

// intialize our express app
const app = express();

// Parse incoming requests data (https://github.com/expressjs/body-parser)
app.use(json());
app.use(urlencoded({ extended: false }));

// end point to add numbers
app.post('/api/add', (req, res) => {
  const { firstNumber, secondNumber } = req.body;
  const result = addNumbers(firstNumber, secondNumber);
  return res.status(200).send({
    result
  });
});

app.get('/api/userdetails', (_, res) => {
  res.status(200).json({ code: 200, data });
});

// app entry point
app.get('/', (req, res) => res.status(200).send({
  message: 'Welcome to our glorious app',
}));

// Setup a default catch-all route that sends back a welcome message in JSON format.
app.get('*', (req, res) => res.status(200).send({
  message: 'Welcome to the beginning of nothingness.',
}));

app.listen(PORT, (err) => {
  if (!err) {
    console.log(`App started on port ${PORT}`);
  } else {
    console.log(err);
  }
});

module.exports = app;